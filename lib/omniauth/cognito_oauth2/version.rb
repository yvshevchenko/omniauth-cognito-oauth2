# frozen_string_literal: true

module OmniAuth
  module CognitoOauth2
    VERSION = '0.1.5'
  end
end
